/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/18 10:45:52 by jrinna            #+#    #+#             */
/*   Updated: 2021/09/19 19:32:45 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/ft_rush02.h"

int	ft_number_only(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (!(str[i] > 47 && str[i] < 58))
			return (0);
		i++;
	}
	return (1);
}

void	ft_print(t_number *bdd)
{
	char	**b;
	int		i;
	char	**c;

	i = 0;
	while (bdd[i].id)
	{
		b = ft_format_entry(bdd[i].value);
		c = ft_tab(b[i]);
		i++;
	}	
	i = 0;
	while (c[i])
	{
		printf("%s\n", c[i]);
		i++;
	}
	free(b);
}

void	ft_exec(int ac, char **av)
{
	t_data		data;
	t_number	*bdd;

	if ((ac == 2 && ft_verif_dico(PATH, &data) == 0)
		|| ac == 3 && ft_verif_dico(av[1], &data) == 0)
		return (0);
	if (!ft_check_parthing_struct(data, bdd))
	{
		ft_put_error(ERROR);
		return (0);
	}
	
	ft_format_entry(bdd->value);
	ft_tab();
	// Appeler le solveur 
}

int	main(int argc, char **argv)
{
	if (argc == 2)
	{
		if (!ft_number_only(argv[1]))
			ft_put_error(ARGS_ERROR);
		else
			ft_exec(argc, argv);
	}
	if (argc == 3)
	{
		if (!ft_number_only(argv[2]))
			ft_put_error(ARGS_ERROR);
		else
			ft_exec(argc, argv);
	}
	else
		ft_put_error(ARGS_ERROR);
	return (0);
}
